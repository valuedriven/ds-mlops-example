# mlops-poc-dvc

## Setup environment

Create requirements.txt

Create virtual environment named `mlops-poc-dvc-env` (you may use other name)
```bash
export ENV_PATH=.mlops-poc-dvc-env
python3.11 -m venv $ENV_PATH
echo "export PYTHONPATH=$PWD" >> $ENV_PATH/bin/activate
source $ENV_PATH/bin/activate
```
Install python libraries

```bash
pip install --upgrade pip setuptools wheel
pip install -r requirements.txt
```
Add Virtual Environment to Jupyter Notebook

```bash
python3 -m ipykernel install --user --name=mlops-poc-dvc-env
```

## DVC setup

```bash
mkdir -p data/{raw,processed}
mkdir models
dvc init
dvc config core.autostage true (enable autostaging)
dvc repro
```

## S3 setup

### Criação de bucket
Criação de bucket com acesso público e versionamento habilitado.

### setup de credenciais AWS
```bash
export AWS_ACCESS_KEY_ID=****
export AWS_SECRET_ACCESS_KEY=****
export AWS_SESSION_TOKEN=****
```

###Configuração do S3 remote
```bash
dvc remote add -d dvc-ml s3://dvc-ml
dvc remote modify dvc-ml version_aware true
```

## Ceph setup

### Criação de bucket
Criação de bucket com acesso público
Execução de script para habilitar versionamento
```bash
python scripts/enable_ceph_versioning.py
```

### setup de credenciais Estaleiro
```bash
export AWS_ACCESS_KEY_ID=****
export AWS_SECRET_ACCESS_KEY=****
```

###Configuração do Ceph remote
```bash
dvc remote add -d mlops-dvc s3://dvcbucket
dvc remote modify mlops-dvc endpointurl https://storagegw.estaleiro.serpro.gov.br
dvc remote modify mlops-dvc version_aware true
```

## Sincronizar repositório remoto


# Diversos
```bash
pylint src
```

dvc push

## TODO:
concluir refatoração do notebook (extrair treino e avaliação)
corrigir função de custo
tratar parametrização - pesquisar config com tipos 'complexos' (ex.: range(1,9))
corrigir columns replace
refatorar lógica de coleta de métricas
refatorar lógica de experimentações (múltiplos modelos?)
exportar params
