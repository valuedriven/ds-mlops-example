from typing import Dict
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC


class UnsupportedClassifier(Exception):
    def __init__(self, estimator_name):
        self.msg = f'Unsupported estimator {estimator_name}'
        super().__init__(self.msg)


def get_supported_estimator() -> Dict:
    return {
        'logreg': LogisticRegression,
        'svm': SVC,
        'knn': KNeighborsClassifier
    }


def train(x_train: pd.DataFrame, y_train: np.array,
          estimator_name: str, param_grid: Dict, **grid_search_cv_params: Dict):

    estimators = get_supported_estimator()

    if estimator_name not in estimators:
        raise UnsupportedClassifier(estimator_name)

    param_grid = hack_param_grid(estimator_name, param_grid)

    estimator = estimators[estimator_name]()
    clf = GridSearchCV(estimator=estimator,
                       param_grid=param_grid,
                       **grid_search_cv_params)

    clf.fit(x_train, y_train)
    estimator.set_params(**clf.best_params_)
    estimator.fit(x_train, y_train)
    return estimator


def hack_param_grid(estimator: str, param_grid):
    if estimator != 'knn':
        return param_grid
    n_neighbors_initial = param_grid['n_neighbors_initial']
    n_neighbors_final = param_grid['n_neighbors_final']
    param_grid = {'n_neighbors': range(n_neighbors_initial, n_neighbors_final)}
    return param_grid
