import yaml


def load_config(config_file='params.yaml'):
    with open(config_file, encoding="utf-8") as conf_file:
        config = yaml.safe_load(conf_file)
    return config
