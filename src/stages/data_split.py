import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from src.utils.logs import get_logger
from src.utils.load_config import load_config
from src.utils.get_args import get_args


def data_split(config_file='params.yaml'):
    config = load_config(config_file)
    x_train_file = config["data"]["x_trainset_path"]
    y_train_file = config["data"]["y_trainset_path"]
    x_test_file = config["data"]["x_testset_path"]
    y_test_file = config["data"]["y_testset_path"]
    logger = get_logger('DATA_SPLIT', log_level=config['base']['log_level'])

    logger.info('Read features')
    dataset = pd.read_csv(config["data"]["features_dataset"])
    logger.info(dataset.shape)

    logger.info('Separate features and target')
    data = dataset.drop(columns='risk')
    logger.info(data.shape)
    target = dataset['risk']
    logger.info(target.shape)

    logger.info('Normalize features')
    scaler = StandardScaler()
    data = scaler.fit_transform(data)

    logger.info('Split features into train and test sets')
    x_train, x_test, y_train, y_test = train_test_split(
        data,
        target,
        test_size=config["data"]["test_size"],
        random_state=config["base"]["random_state"])

    logger.info('Save train and test sets')
    pd.DataFrame(x_train).to_csv(x_train_file, index=False)
    pd.DataFrame(y_train).to_csv(y_train_file, index=False)
    pd.DataFrame(x_test).to_csv(x_test_file, index=False)
    pd.DataFrame(y_test).to_csv(y_test_file, index=False)

    return x_train, x_test, y_train, y_test


if __name__ == '__main__':
    args = get_args()
    data_split(config_file=args.config)
