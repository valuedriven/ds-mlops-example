import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from src.utils.logs import get_logger
from src.utils.load_config import load_config
from src.utils.get_args import get_args


def feature_data(config_file='params.yaml'):
    config = load_config(config_file)
    local_dataset = config["data"]["local_dataset"]
    features_dataset = config["data"]["features_dataset"]
    log_level = config['base']['log_level']
    logger = get_logger('FEATURE_DATA', log_level=log_level)

    logger.info('Read local dataset')
    dataset = pd.read_csv(local_dataset)

    logger.info('Rename target name')
    dataset.rename(columns={"class": "risk"}, inplace=True)

    logger.info('Extract features names')
    category_features = dataset.select_dtypes(['object']).columns.to_numpy()
    category_features = np.delete(
        category_features, np.where(category_features == 'risk'))
    numeric_features = dataset.select_dtypes(['int64']).columns.to_numpy()

    logger.info('Encode target to numbers')
    encoder = LabelEncoder()
    dataset['risk'] = encoder.fit_transform(dataset['risk'])

    logger.info('Encode categorical features to numbers')
    encoder = OneHotEncoder(handle_unknown='error', sparse_output=False)
    categories_dataset = pd.DataFrame(
        encoder.fit_transform(dataset[category_features]))
    categories_dataset.columns = encoder.get_feature_names_out()

    logger.info('Join categorical and numeric features')
    data = pd.concat([dataset[numeric_features], categories_dataset], axis=1)

    logger.info('Join target to the dataset')
    data = pd.concat([data, dataset['risk']], axis=1)

    logger.info('Adjust features names')
    dataset.columns = [colname.lower().replace('[<,>,=,#,@,&,(,),\', ,/,\t]', '_')
                       for colname in dataset.columns.tolist()]

    logger.info('Save feature dataset')
    data.to_csv(features_dataset, index=False)
    return data


if __name__ == '__main__':
    args = get_args()
    feature_data(config_file=args.config)
