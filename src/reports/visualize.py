import itertools
import matplotlib.colors
import matplotlib.pyplot as plt
import numpy as np


def plot_confusion_matrix(confusion_matrix: np.array,
                          target_names: list[str],
                          title: str = 'Confusion matrix',
                          cmap: matplotlib.colors.LinearSegmentedColormap = None,
                          normalize: bool = True):

    accuracy = np.trace(confusion_matrix) / float(np.sum(confusion_matrix))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(confusion_matrix, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        confusion_matrix = confusion_matrix.astype(
            'float') / confusion_matrix.sum(axis=1)[:, np.newaxis]

    thresh = confusion_matrix.max() / 1.5 if normalize else confusion_matrix.max() / 2
    for i, j in itertools.product(
            range(confusion_matrix.shape[0]),
            range(confusion_matrix.shape[1])):
        if normalize:
            plt.text(j, i, f'{confusion_matrix[i, j]:0.4f}',
                     horizontalalignment="center",
                     color="white" if confusion_matrix[i, j] > thresh else "black")
        else:
            plt.text(j, i, f'{confusion_matrix[i, j]:,}',
                     horizontalalignment="center",
                     color="white" if confusion_matrix[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel(
        f'Predicted label\naccuracy={accuracy:0.4f}; misclass={misclass:0.4f}')

    return plt.gcf()
