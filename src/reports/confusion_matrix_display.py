from dataclasses import dataclass
import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay


@dataclass
class ConfusionMatrixParams:
    options: str
    method: str
    display_labels: str
    color_map: str


def confusion_matrix_display(y_test, y_pred, params: ConfusionMatrixParams):
    for title, normalize in params.options:
        disp = ConfusionMatrixDisplay.from_predictions(
            y_test, y_pred,
            display_labels=params.display_labels,
            normalize=normalize,
            cmap=params.color_map)
        disp.ax_.set_title(params.method+' -  '+title)
    plt.show()
